$(document).ready(function(){
	$("#tdfc .left-scroll").bind("click", function(){
		moveleftright($("#tdfc .scroll-container"), 150, 6, "right")
	});
	$("#tdfc .right-scroll").bind("click", function(){
		moveleftright($("#tdfc .scroll-container"), 150, 6, "left")
	});
	function moveleftright(containerEle, singleWidth, visibleNum, direction){
		var totalNum = containerEle.children("li").length;
		var maxLeft = (totalNum-visibleNum)*singleWidth;
		var left = parseInt(containerEle.css("left"));
		if(direction == "left"){
			left-=singleWidth;
		}else{
			left+=singleWidth;
		}
		if(left > 0)left =0;
		if(left < -maxLeft) left = -maxLeft;
		containerEle.css("left", left)
	}
	$("#caseshow .sub-nav li").hover(
		function(){
			$("#caseshow .sub-nav li").removeClass("actived");
			$(this).addClass("actived");
			$("#caseshow .content").removeClass("content-actived");
			$("#caseshow ."+$(this).attr("cat")).addClass("content-actived");
		}
	)
})